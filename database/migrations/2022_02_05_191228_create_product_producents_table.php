<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductProducentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_producents', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('producent_id')->nullable()->after('category_id');
            $table->foreign('producent_id')->references('id')->on('product_producents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_producent_id_foreign');
            $table->dropColumn('producent_id');
        });
        Schema::dropIfExists('product_producents');
    }
}
