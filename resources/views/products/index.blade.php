@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Lista produktów</h1>
            </div>
            <div class="col-6">
                <a class="float-right" href="{{ route('products.create') }}">
                    <button type="button" class="btn btn-primary">Dodaj</button>
                </a>
            </div>
        </div>
        <div class="row">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Opis</th>
                    <th scope="col">Ilość</th>
                    <th scope="col">Cena</th>
                    <th scope="col">Kategoria</th>
                    <th scope="col">Wielkość</th>
                    <th scope="col">Producent</th>
                    <th scope="col">Akcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <th scope="row">{{ $product->id }}</th>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->opis }}</td>
                        <td>{{ $product->ilosc }}</td>
                        <td>{{ $product->cena }}</td>
                        <td>@if(!is_null($product->category)){{ $product->category->name }}@endif</td>
                        <td>@if(!is_null($product->size)){{ $product->size->name }}@endif</td>
                        <td>@if(!is_null($product->producent)){{ $product->producent->name }}@endif</td>
                        <td>
                            <a href="{{ route('products.show', $product->id) }}">
                                <button class="btn btn-primary btn-sm">Produkt</button>
                            </a>
                            <a href="{{ route('products.edit', $product->id) }}">
                                <button class="btn btn-success btn-sm">Edytuj</button>
                            </a>
                            <button class="btn btn-danger btn-sm delete" data-id="{{ $product->id }}">
                                X
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('javascript')
    const deleteUrl = "{{ url('products') }}/";
@endsection
@section('js-files')
    <script src="{{ asset('js/delete.js') }}"></script>
@endsection
