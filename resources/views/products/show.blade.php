@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Podgląd produktu</div>

                <div class="card-body">
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Nazwa</label>

                            <div class="col-md-6">
                                <input id="name" type="text" maxlength="500" class="form-control" name="name" value="{{ $product->name }}" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="opis" class="col-md-4 col-form-label text-md-end">Opis</label>

                            <div class="col-md-6">
                                <textarea id="opis" type="text" maxlength="1500" class="form-control" name="opis" disabled>{{ $product->opis }}</textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="ilosc" class="col-md-4 col-form-label text-md-end">Ilość</label>

                            <div class="col-md-6">
                                <input id="ilosc" type="number" min="0" class="form-control" name="ilosc" value="{{ $product->ilosc }}" disabled>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="cena" class="col-md-4 col-form-label text-md-end">Cena</label>
                            <div class="col-md-6">
                                <input id="cena" type="number"  step="0.01" min="0" class="form-control" name="cena" value="{{ $product->cena }}" disabled>
                            </div>
                        </div>

                    <div class="row mb-3">
                        <label for="category" class="col-md-4 col-form-label text-md-end">Kategoria</label>

                        <div class="col-md-6">
                            <select id="cena" class="form-control" name="category_id" disabled>
                                @if(!is_null($product->category))
                                    <option>{{$product->category->name}}</option>
                                @else
                                    <option>Brak</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="category" class="col-md-4 col-form-label text-md-end">Wielkość</label>

                        <div class="col-md-6">
                            <select id="cena" class="form-control" name="category_id" disabled>
                                @if(!is_null($product->size))
                                    <option>{{$product->size->name}}</option>
                                @else
                                    <option>Brak</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="category" class="col-md-4 col-form-label text-md-end">Producent</label>

                        <div class="col-md-6">
                            <select id="cena" class="form-control" name="category_id" disabled>
                                @if(!is_null($product->producent))
                                    <option>{{$product->producent->name}}</option>
                                @else
                                    <option>Brak</option>
                                @endif
                            </select>
                        </div>
                    </div>

                        <div class="row mb-3">
                            <label for="image" class="col-md-4 col-form-label text-md-end">Grafika</label>

                            <div class="col-md-6 justify-content-center">
                                <img src="{{asset('storage/' . $product->image_path)}}" alt="Zdjecie produktu">
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
