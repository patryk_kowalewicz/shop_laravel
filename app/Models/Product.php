<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_path',
        'name',
        'opis',
        'ilosc',
        'cena',
        'category_id',
        'producent_id',
        'size_id'
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class);
    }
    public function producent(): BelongsTo
    {
        return $this->belongsTo(ProductProducent::class);
    }
    public function size(): BelongsTo
    {
        return $this->belongsTo(ProductSize::class);
    }
}
